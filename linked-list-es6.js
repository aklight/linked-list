class LinkedList {
    constructor() {
        this.head = null;
        this.tail = null;
    }

    addToHead(value) {
        var newNode = new Node(value, this.head, null);
        if (this.head) {
            this.head.prev = newNode;
        } else {
            this.tail = newNode;
        }
        this.head = newNode;
    }

    addToTail(value) {
        var newNode = new Node(value, null, this.tail);

        if (this.tail) {
            this.tail.next = newNode
        } else {
            this.head = newNode;
        }
        this.tail = newNode;
    };

    removeHead() {
        if (!this.head) {
            return null;
        }
        var val = this.head.value;
        this.head = this.head.next;
        if (this.head) {
            this.head.prev = null;
        } else {
            this.tail = null;
        }

        return val;
    }

    removeTail() {
        if (!this.tail) {
            return null;
        }
        let val = this.tail.value;
        this.tail = this.tail.prev;
        if (this.tail) {
            this.tail.next = null;
        } else {
            this.head = null;
        }
        return val;
    }

    search(searchValue) {
        var currentNode = this.head;
        while (currentNode) {
            if (currentNode.value === searchValue) {
                return currentNode.value;
            }
            currentNode = currentNode.next;
        }
        return null;
    }

    indexOf(value) {
        var currentNode = this.head;
        var arr = [];
        var index = 0;
        while (currentNode) {
            if (currentNode.value === value) {
                arr.push(index);
            }
            currentNode = currentNode.next;
            index++;
        }
        return arr;
    }

}

class Node {
    constructor(value, next, prev) {
        this.value = value;
        this.next = next;
        this.prev = prev;
    }

}

const myLL = new LinkedList();
myLL.addToHead(5);
myLL.addToHead(5);
myLL.addToHead('hello');
myLL.addToTail(19);
myLL.addToTail('world');
myLL.addToTail(5);
console.log(myLL)
console.log(myLL.indexOf(5))